#include <tf/transform_listener.h>

int main(int argc, char **argv)
{
    ros::init(argc,argv,"colision_detect");

    tf::TransformListener listener;
    tf::StampedTransform transform;
    ros::Rate rate(10); //10hz

    while(ros::ok())
    {
        try
        {
            listener.lookupTransform("turtle1", "turtle2",ros::Time(0), transform);
            if(transform.getOrigin().length()<1)
                ROS_INFO("Kolizia zolwi!!!");
        }
        catch(tf::TransformException &ex)
        {
            ROS_INFO("error");
            ros::Duration(1).sleep();   //sleep 1s
        }
        rate.sleep();
    }
}
