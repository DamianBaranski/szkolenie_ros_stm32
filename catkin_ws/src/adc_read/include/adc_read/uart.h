/*
 *  Created on: Feb 18, 2016
 *      Author: Damian Barański
 */

void serial_port_close(int serial_port);
int serial_port_open(void);
int serial_port_read(int serial_port, char *read_buffer, int max_chars_to_read);
void serial_port_write(int serial_port,const char *write_buffer);
