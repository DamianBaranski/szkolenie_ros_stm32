#include <adc_read/uart.h>
#include <ros/ros.h>
#include <std_msgs/Float64.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "adc_read");

    ros::NodeHandle nh;
    ros::Publisher pub = nh.advertise<std_msgs::Float64>("/adc",1000);
    ros::Rate rate(10000);
    std_msgs::Float64 msg;
    int uart_handle=serial_port_open();
    char buff[128];
    while(ros::ok())
    {

        if(serial_port_read(uart_handle,buff,128)>0)
        {
            int value=atoi(buff);
            msg.data=value*3.3/4092.0;
            pub.publish(msg);

        }

        rate.sleep();
    }

}
