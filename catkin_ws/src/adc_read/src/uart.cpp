/*
 *  Created on: Feb 18, 2016
 *      Author: Damian Barański
 */
#include <adc_read/uart.h>
#include <fcntl.h>      //O_RDWR, O_NONBLOCK
#include <termios.h>
#include <cstring>      //strlen()
#include <unistd.h>     //close(), read(), write(), open()
#include <iostream>     //cout, cerr, endl


using namespace std;

// Resets the terminal and closes the serial port.
void serial_port_close(int serial_port)
{
  close(serial_port);
}

// Opens a USB virtual serial port at ttyUSB0.
// returns - the port's file descriptor or -1 on error.
int serial_port_open(void)
{
  struct termios options;

  int serial_port = open("/dev/ttyUSB0", O_RDWR | O_NONBLOCK );

  if (serial_port != -1)
  {
    cout << "Serial Port open" << endl;
    tcgetattr(serial_port, &options);
    cfsetispeed(&options, B115200);
    cfsetospeed(&options, B115200);
    options.c_cflag |= (CLOCAL | CREAD);
    options.c_lflag |= ICANON;
    tcsetattr(serial_port, TCSANOW, &options);
  }
  else
    cerr << "Unable to open /dev/ttyUSB0" << endl;
  return (serial_port);
}

// returns - 0 if data was received, -1 if no data received.
int serial_port_read(int serial_port, char *read_buffer, int max_chars_to_read)
{
  int chars_read = read(serial_port, read_buffer, max_chars_to_read);
  read_buffer[chars_read]=0;
  return chars_read;
}

void serial_port_write(int serial_port,const char *write_buffer)
{
  int bytes_written;
  int len = 0;
  len = strlen(write_buffer);
  bytes_written = write(serial_port, write_buffer, len);
  if (bytes_written < len)
  {
    cerr << "Write failed" << endl;
  }
}

