#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <rqt_wykres/wykres.h>


using std::cout;
using std::endl;

namespace rqt_wykres {
wykres::wykres(): rqt_gui_cpp::Plugin(), widget_(0)
{
    setObjectName("wykres");
}

void wykres::initPlugin(qt_gui_cpp::PluginContext& context)
{
    widget_ = new QWidget();
    ui_.setupUi(widget_);
    if (context.serialNumber() > 1)
    {
        widget_->setWindowTitle(widget_->windowTitle() + " (" + QString::number(context.serialNumber()) + ")");
    }
    context.addWidget(widget_);
    ui_.Plot->addGraph();
    ui_.Plot->graph(0)->setAntialiased(true);
    ui_.Plot->graph(0)->setAdaptiveSampling(true);
    ui_.Plot->xAxis->grid()->setVisible(false);

    sub=nh.subscribe("/adc",1000,&wykres::SubCallback,this);
    //http://doc.qt.io/qt-4.8/qt.html#ConnectionType-enum
    connect(this,SIGNAL(addData(double)),this,SLOT(drawData(double)),Qt::BlockingQueuedConnection);
    time0=ros::Time::now().sec+ros::Time::now().nsec/1000000000.0;
}

void wykres::shutdownPlugin()
{
    ROS_INFO_STREAM("Zamykam");
    sub.shutdown();

    ui_.Plot->graph(0)->clearData();
    exit(0);
}

void wykres::saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const
{
}

void wykres::restoreSettings(const qt_gui_cpp::Settings& plugin_settings, const qt_gui_cpp::Settings& instance_settings)
{
}

void wykres::SubCallback(const std_msgs::Float64ConstPtr &msg)
{
    //ROS_INFO_STREAM("Odebrałem dane");
    emit addData(msg->data);
}

void wykres::drawData(double data)
{
    //ROS_INFO_STREAM("Ja tez dostałem");
    double time=ros::Time::now().sec+ros::Time::now().nsec/1000000000.0-time0;
    //ROS_INFO_STREAM("Czas to : " << time);
    ui_.Plot->graph(0)->addData(time,data);
    ui_.Plot->xAxis->setRange(time-60,time);
    ui_.Plot->replot();

}
}

PLUGINLIB_EXPORT_CLASS(rqt_wykres::wykres, rqt_gui_cpp::Plugin)
