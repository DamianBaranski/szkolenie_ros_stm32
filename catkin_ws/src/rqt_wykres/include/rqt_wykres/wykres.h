#ifndef rqt_wykres__wykres_H
#define rqt_wykres__wykres_H

// ROS headers
#include <ros/ros.h>
#include <rqt_gui_cpp/plugin.h>

// Qt widget header
#include <ui_wykres.h>
#include <ros/ros.h>
#include <std_msgs/Float64.h>


namespace rqt_wykres {

class wykres: public rqt_gui_cpp::Plugin
{
    Q_OBJECT

public:
    wykres();
    virtual void initPlugin(qt_gui_cpp::PluginContext& context);
    virtual void shutdownPlugin();
    virtual void saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const;
    virtual void restoreSettings(const qt_gui_cpp::Settings& plugin_settings, const qt_gui_cpp::Settings& instance_settings);

    void SubCallback(const std_msgs::Float64ConstPtr &msg);

protected slots:
    void drawData(double data);

signals:
    void addData(double data);

protected:
    Ui::wykresWidget ui_;
    QWidget* widget_;
    double time0;
    ros::NodeHandle nh;
    ros::Subscriber sub;
};
}

#endif // rqt_wykres__wykres_H
