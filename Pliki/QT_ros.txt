Instrukcja uruchomienia QT dla ROS-a.

Należy wykonać gdy dostajemy informację że qt nie znajduje CMake, lub podobne błędy.

należy utworzyć plik <dowolna nazwa>.desktop
i wpisać do niego:

[Desktop Entry]
Exec=bash -i -c <ścieższka do qt creatora>/qtcreator %F
Icon=<scieższka do pliku ikony>/<plik ikony np png>
Type=Application
Terminal=false
Name=Qt for ROS 

zapisujemy i nadajemy prawo wykonywania przy użyciu polecenia:
chmod +x <dowolna nazwa>.desktop

po uruchomieniu QT z naszego skrótu będzie już działać QT z ROS-em
(uruchamiamy przy pomocy dwukiliku na naszym plik)

