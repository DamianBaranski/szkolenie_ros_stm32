#ifndef rqt_(>>>APP-NAME<<<)__(>>>APP-NAME<<<)_H
#define rqt_(>>>APP-NAME<<<)__(>>>APP-NAME<<<)_H

// ROS headers
#include <ros/ros.h>
#include <rqt_gui_cpp/plugin.h>

// Qt widget header
#include <ui_(>>>APP-NAME<<<).h>

namespace rqt_(>>>APP-NAME<<<) {

  class (>>>APP-NAME<<<): public rqt_gui_cpp::Plugin
  {
    Q_OBJECT

    public:
    (>>>APP-NAME<<<)();
    virtual void initPlugin(qt_gui_cpp::PluginContext& context);
    virtual void shutdownPlugin();
    virtual void saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const;
    virtual void restoreSettings(const qt_gui_cpp::Settings& plugin_settings, const qt_gui_cpp::Settings& instance_settings);

    protected slots:
    
    protected:
    Ui::(>>>APP-NAME<<<)Widget ui_;
    QWidget* widget_;
  };
}

#endif // rqt_(>>>APP-NAME<<<)__(>>>APP-NAME<<<)_H
