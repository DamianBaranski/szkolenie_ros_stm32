#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <rqt_(>>>APP-NAME<<<)/(>>>APP-NAME<<<).h>

using std::cout;
using std::endl;

namespace rqt_(>>>APP-NAME<<<) {
  (>>>APP-NAME<<<)::(>>>APP-NAME<<<)(): rqt_gui_cpp::Plugin(), widget_(0)
  {
    setObjectName("(>>>APP-NAME<<<)");
  }

  void (>>>APP-NAME<<<)::initPlugin(qt_gui_cpp::PluginContext& context)
  {
    widget_ = new QWidget();
    ui_.setupUi(widget_);
    if (context.serialNumber() > 1)
    {
      widget_->setWindowTitle(widget_->windowTitle() + " (" + QString::number(context.serialNumber()) + ")");
    }
    context.addWidget(widget_);
  }
  
  void (>>>APP-NAME<<<)::shutdownPlugin()
  {
  }
  
  void (>>>APP-NAME<<<)::saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const
  {
  }

  void (>>>APP-NAME<<<)::restoreSettings(const qt_gui_cpp::Settings& plugin_settings, const qt_gui_cpp::Settings& instance_settings)
  {
  }
}

PLUGINLIB_EXPORT_CLASS(rqt_(>>>APP-NAME<<<)::(>>>APP-NAME<<<), rqt_gui_cpp::Plugin)
